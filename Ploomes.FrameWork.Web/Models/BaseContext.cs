﻿using System.Data.Entity;

namespace Ploomes.FrameWork.Web.Models
{
    public class BaseContext : DbContext
    {
        public BaseContext(): base("BaseContext")
        {
            Database.SetInitializer<BaseContext>(null);           
        }

        public DbSet<Optin> Optin { get; set; }
    }
}