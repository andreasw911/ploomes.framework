﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ploomes.FrameWork.Web.Models
{    
    [Table("Optin")]
    public class Optin
    {
        [Key]        
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }       
    }
}