﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Ploomes.FrameWork.Web.Models;

namespace Ploomes.FrameWork.Web.Controllers
{
    public class OptinsController : ApiController
    {
        private BaseContext db = new BaseContext();


        // GET: api/Optins
        public IQueryable<Optin> GetOptin()
        {
            return db.Optin;            
        }

        // GET: api/Optins/5
        [ResponseType(typeof(Optin))]
        public IHttpActionResult GetOptin(int id)
        {
            Optin optin = db.Optin.Find(id);
            if (optin == null)
            {
                return NotFound();
            }

            return Ok(optin);
        }

        // PUT: api/Optins/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOptin(int id, Optin optin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != optin.Id)
            {
                return BadRequest();
            }

            db.Entry(optin).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OptinExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Optins
        [ResponseType(typeof(Optin))]
        public IHttpActionResult PostOptin(Optin optin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Optin.Add(optin);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = optin.Id }, optin);
        }

        // DELETE: api/Optins/5
        [ResponseType(typeof(Optin))]
        public IHttpActionResult DeleteOptin(int id)
        {
            Optin optin = db.Optin.Find(id);
            if (optin == null)
            {
                return NotFound();
            }

            db.Optin.Remove(optin);
            db.SaveChanges();

            return Ok(optin);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OptinExists(int id)
        {
            return db.Optin.Count(e => e.Id == id) > 0;
        }
    }
}